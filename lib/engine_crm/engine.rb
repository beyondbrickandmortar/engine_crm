module EngineCrm
  class Engine < ::Rails::Engine
    isolate_namespace EngineCrm
  end

  mattr_accessor :current_user_method
end
