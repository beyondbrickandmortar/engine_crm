$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'engine_crm/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'engine_crm'
  s.version     = EngineCrm::VERSION
  s.authors     = ['Mike Cochran']
  s.email       = ['mcochran@linux.com']
  s.homepage    = 'http://vongrippen.com'
  s.summary     = 'A simple CRM implemented as a mountable Rails 3.2 engine.'
  s.description = 'A simple CRM implemented as a mountable Rails 3.2 engine.'

  s.files = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'rails', '~> 3.2.0'
  s.add_dependency 'activerecord-postgres-hstore', '~> 0.7.6'
  s.add_dependency 'pg', '~> 0.16.0'
  s.add_dependency 'ransack', '~> 1.0.0'
  s.add_dependency 'icalendar', '~> 1.4.1'

  s.add_development_dependency 'thin'
end
