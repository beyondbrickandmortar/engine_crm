EngineCrm::Engine.routes.draw do
  resources :lists do
  	post 'add'
  	delete 'remove'
  end


  resources :calendar, only: [:show]

  resources :companies do
    resources :communications
  end
  resources :people, path: 'contacts'
  resources :communications
  root to: 'activities#index'
end
