class RenameContactIdToPersonIdOnCommunication < ActiveRecord::Migration
  def change
    rename_column :engine_crm_communications, :contact_id, :person_id
  end
end
