class CreateEngineCrmContacts < ActiveRecord::Migration
  def change
    create_table :engine_crm_contacts do |t|
      t.text :name
      t.text :phone
      t.text :city
      t.text :state
      t.text :zip
      t.text :notes

      t.timestamps
    end
  end
end
