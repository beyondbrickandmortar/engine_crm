class CreateEngineCrmContactsEngineCrmLists < ActiveRecord::Migration
  def change
  	create_table :engine_crm_contacts_engine_crm_lists, id: false do |t|
      t.references :contact, index: true
      t.references :list, index: true
    	t.string :contact_type
    end
  end
end
