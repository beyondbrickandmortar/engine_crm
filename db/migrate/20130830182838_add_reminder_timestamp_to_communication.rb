class AddReminderTimestampToCommunication < ActiveRecord::Migration
  def change
    add_column :engine_crm_communications, :reminder_timestamp, :datetime
  end
end
