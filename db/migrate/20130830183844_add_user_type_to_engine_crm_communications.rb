class AddUserTypeToEngineCrmCommunications < ActiveRecord::Migration
  def change
    add_column :engine_crm_communications, :user_type, :string
  end
end
