class CreateEngineCrmLists < ActiveRecord::Migration
  def change
    create_table :engine_crm_lists do |t|
      t.string :name

      t.timestamps
    end
  end
end
