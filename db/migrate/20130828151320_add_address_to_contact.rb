class AddAddressToContact < ActiveRecord::Migration
  def change
    add_column :engine_crm_contacts, :address, :text
  end
end
