class AddUserRefToCommunication < ActiveRecord::Migration
  def change
    add_column :engine_crm_communications, :user_id, :binary
  end
end
