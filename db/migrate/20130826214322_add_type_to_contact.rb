class AddTypeToContact < ActiveRecord::Migration
  def change
    add_column :engine_crm_contacts, :type, :string
  end
end
