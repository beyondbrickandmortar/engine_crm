class CreateEngineCrmCommunications < ActiveRecord::Migration
  def change
    create_table :engine_crm_communications do |t|
      t.datetime :timestamp
      t.references :contact, index: true
      t.references :company, index: true
      t.text :details

      t.timestamps
    end
  end
end
