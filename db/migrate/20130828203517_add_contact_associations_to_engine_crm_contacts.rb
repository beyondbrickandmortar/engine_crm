class AddContactAssociationsToEngineCrmContacts < ActiveRecord::Migration
  def change
    add_column :engine_crm_contacts, :contact_id, :integer
    add_index :engine_crm_contacts, :contact_id
    add_column :engine_crm_contacts, :contact_type, :string
  end
end
