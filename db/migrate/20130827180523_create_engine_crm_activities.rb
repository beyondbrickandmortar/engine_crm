class CreateEngineCrmActivities < ActiveRecord::Migration
  def change
    create_table :engine_crm_activities do |t|
      t.binary :user_id
      t.string :action
      t.binary :trackable
      t.string :trackable_type

      t.timestamps
    end
  end
end
