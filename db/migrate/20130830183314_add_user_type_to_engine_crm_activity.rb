class AddUserTypeToEngineCrmActivity < ActiveRecord::Migration
  def change
    add_column :engine_crm_activities, :user_type, :string
  end
end
