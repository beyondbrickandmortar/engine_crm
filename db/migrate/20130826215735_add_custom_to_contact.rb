class AddCustomToContact < ActiveRecord::Migration
  def change
    add_column :engine_crm_contacts, :custom, :hstore
  end
end
