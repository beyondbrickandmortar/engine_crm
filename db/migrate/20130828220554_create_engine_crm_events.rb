class CreateEngineCrmEvents < ActiveRecord::Migration
  def change
    create_table :engine_crm_events do |t|
      t.binary :user_id
      t.string :user_type
      t.text :title
      t.datetime :start
      t.datetime :end
      t.text :description

      t.timestamps
    end
  end
end
