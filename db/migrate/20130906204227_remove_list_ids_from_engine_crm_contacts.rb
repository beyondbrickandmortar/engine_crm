class RemoveListIdsFromEngineCrmContacts < ActiveRecord::Migration
  def up
    remove_column :engine_crm_contacts, :list_id
  end

  def down
    add_column :engine_crm_contacts, :list_id, :integer
    add_index :engine_crm_contacts, :list_id
  end
end
