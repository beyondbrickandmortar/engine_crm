class RenameTrackableToTrackableIdInEngineCrmActivity < ActiveRecord::Migration
  def change
  	rename_column :engine_crm_activities, :trackable, :trackable_id
  end
end
