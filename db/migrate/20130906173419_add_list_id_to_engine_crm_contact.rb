class AddListIdToEngineCrmContact < ActiveRecord::Migration
  def change
    add_column :engine_crm_contacts, :list_id, :integer
    add_index :engine_crm_contacts, :list_id
  end
end
