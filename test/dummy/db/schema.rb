# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130906204506) do

  create_table "activities", :force => true do |t|
    t.binary   "user_id"
    t.string   "user_type"
    t.string   "action"
    t.binary   "trackable_id"
    t.string   "trackable_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "engine_crm_activities", :force => true do |t|
    t.binary   "user_id"
    t.string   "action"
    t.binary   "trackable_id"
    t.string   "trackable_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "user_type"
  end

  create_table "engine_crm_communications", :force => true do |t|
    t.datetime "timestamp"
    t.integer  "person_id"
    t.integer  "company_id"
    t.text     "details"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.binary   "user_id"
    t.datetime "reminder_timestamp"
    t.string   "user_type"
  end

# Could not dump table "engine_crm_contacts" because of following StandardError
#   Unknown type 'hstore' for column 'custom'

  create_table "engine_crm_contacts_engine_crm_lists", :id => false, :force => true do |t|
    t.integer "contact_id"
    t.string  "contact_type"
  end

  create_table "engine_crm_events", :force => true do |t|
    t.binary   "user_id"
    t.string   "user_type"
    t.text     "title"
    t.datetime "start"
    t.datetime "end"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "engine_crm_lists", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
