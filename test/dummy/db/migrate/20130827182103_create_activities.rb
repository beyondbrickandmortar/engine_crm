class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.binary :user_id
      t.string :user_type
      t.string :action
      t.binary :trackable_id
      t.string :trackable_type

      t.timestamps
    end
  end
end
