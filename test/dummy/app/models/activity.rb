class Activity < ActiveRecord::Base
  attr_accessible :action, :trackable_id, :trackable_type, :user_id, :user_type
end
