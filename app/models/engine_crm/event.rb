module EngineCrm
  class Event < ActiveRecord::Base
    serialize :user_id
    attr_accessible :description, :end, :start, :title, :user

    def user
      self.user_type.find(self.user_id) rescue nil
    end

    def user=(usr)
      self.user_id = usr.id rescue nil
      self.user_type = usr.class.to_s
    end

    def user_type
      super.constantize
    end
  end
end
