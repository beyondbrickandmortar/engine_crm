module EngineCrm
  class Person < EngineCrm::Contact
    has_many :communications, class_name: 'EngineCrm::Communication'

    validates_format_of :email, with: /@/, allow_nil: true, allow_blank: true

    CUSTOM_ATTRS=['title', 'email', 'cell_phone']

    CUSTOM_ATTRS.each do |attr|
      attr_accessible attr.to_sym

      self.class_eval "
        def #{attr}
          self.custom['#{attr}']
        end

        def #{attr}=(a)
          self.custom['#{attr}']=a
        end
      "
    end
  end
end