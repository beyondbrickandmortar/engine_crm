require 'activerecord-postgres-hstore'
require 'ransack'

module EngineCrm
  class Contact < ActiveRecord::Base
    serialize :custom, ActiveRecord::Coders::Hstore
    attr_accessible :name, :notes, :phone, :city, :state, :zip, :custom, :contact, :contact_id, :contact_type, :address
    validates_presence_of :name

    has_many :contacts, as: :contact
    belongs_to :contact

    has_and_belongs_to_many :lists, join_table: 'engine_crm_contacts_engine_crm_lists', foreign_key: 'contact_id', class_name: 'EngineCrm::List'

    before_save :sanitize_phone

    def self.ransackable_attributes(auth_object=nil)
      (column_names - ['id', 'contact_id', 'contact_type', 'created_at', 'updated_at', 'type', 'custom']) + _ransackers.keys
    end

    protected
    def sanitize_phone
      self.phone = self.phone.scan(/\d/).join
    end
  end
end
