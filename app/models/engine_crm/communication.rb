module EngineCrm
  class Communication < ActiveRecord::Base
    belongs_to :person, class_name: 'EngineCrm::Person'
    belongs_to :company, class_name: 'EngineCrm::Company'

    serialize :user_id

    attr_accessible :details, :company_id, :person, :person_id, :user, :enable_reminder, :reminder_text, :reminder_timestamp, :timestamp

    attr_reader :reminder_text, :enable_reminder
    attr_writer :reminder_text, :enable_reminder

    after_save :create_reminder

    def self.ransackable_attributes(auth_object=nil)
      (column_names - ['id', 'contact_id', 'contact_type', 'updated_at', 'type']) + _ransackers.keys
    end

    def user
      self.user_type.find(self.user_id) rescue nil
    end

    def user=(usr)
      self.user_id = usr.id rescue nil
      self.user_type = usr.class.to_s
    end

    def user_type
      self['user_type'].constantize
    end

    def user_type=(str)
      self['user_type'] = str.to_s
    end

    private
    def create_reminder
      if self.user and self.enable_reminder and ![0, '0'].include? self.enable_reminder
        event = EngineCrm::Event.new
        event.start = self.reminder_timestamp
        event.end = event.start + 1.hour
        event.description = self.reminder_text
        event.user_id = self.user_id
        event.user_type = self.user_type.to_s
        event.save
      end
    end
  end
end
