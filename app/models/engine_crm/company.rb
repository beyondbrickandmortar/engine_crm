module EngineCrm
  class Company < EngineCrm::Contact
    has_many :communications, class_name: 'EngineCrm::Communication', dependent: :destroy, order: 'timestamp DESC'

    CUSTOM_ATTRS=['website']

    CUSTOM_ATTRS.each do |attr|
      attr_accessible attr.to_sym

      self.class_eval "
        def #{attr}
          self.custom['#{attr}']
        end

        def #{attr}=(a)
          self.custom['#{attr}']=a
        end
      "
    end
  end
end