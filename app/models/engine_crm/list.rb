module EngineCrm
  class List < ActiveRecord::Base
    attr_accessible :name
		default_scope order('name DESC')

		has_and_belongs_to_many :contacts, join_table: 'engine_crm_contacts_engine_crm_lists', foreign_key: 'list_id'
  end
end
