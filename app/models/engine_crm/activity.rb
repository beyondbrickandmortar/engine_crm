module EngineCrm
  class Activity < ActiveRecord::Base
    serialize :user_id
    serialize :trackable_id

    attr_accessible :user, :trackable

    def user
      self.user_type.find(self.user_id) rescue nil
    end

    def user=(usr)
      self.user_id = usr.id rescue nil
      self.user_type = usr.class.to_s
    end

    def user_type
      super.constantize
    end

    def trackable
      self.trackable_type.find(self.trackable_id) rescue nil
    end

    def trackable=(obj)
      self.trackable_id = obj.id rescue nil
      self.trackable_type = obj.class.to_s
    end

    def trackable_type
      super.constantize
    end
  end
end
