require_dependency 'engine_crm/application_controller'

module EngineCrm
  class CompaniesController < ApplicationController
    before_filter :set_company, only: [:show, :edit, :update, :destroy]

    # GET /companies
    def index
      @search = EngineCrm::Company.search(params[:q])
      @companies = @search.result
      @search.build_condition if @search.conditions.empty?
      @search.build_sort if @search.sorts.empty?
    end

    # GET /companies/1
    def show
    end

    # GET /companies/new
    def new
      @company = EngineCrm::Company.new
    end

    # GET /companies/1/edit
    def edit
    end

    # POST /companies
    def create
      @company = EngineCrm::Company.new(company_params)

      if @company.save
        redirect_to @company, notice: 'Company was successfully created.'
      else
        render action: 'new'
      end
    end

    # PATCH/PUT /companies/1
    def update
      if @company.update_attributes(company_params)
        redirect_to @company, notice: 'Company was successfully updated.'
      else
        render action: 'edit'
      end
    end

    # DELETE /companies/1
    def destroy
      @company.destroy
      redirect_to companies_url, notice: 'Company was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_company
        @company = EngineCrm::Company.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def company_params
        params[:company]
      end
  end
end
