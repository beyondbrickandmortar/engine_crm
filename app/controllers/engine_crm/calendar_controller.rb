require_dependency 'engine_crm/application_controller'
require 'icalendar'
require 'date'

module EngineCrm
  class CalendarController < ApplicationController
    include Icalendar
    respond_to :ics
    def show
      cal = Calendar.new

      @events = EngineCrm::Event.where(user_id: params[:id]).all

      @events.each do |event|
        cal.event do
          dtstart event.start
          dtend event.end
          summary event.title
          description event.description
        end
      end

      render text: cal.to_ical
    end
  end
end
