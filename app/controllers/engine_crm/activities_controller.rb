require_dependency 'engine_crm/application_controller'

module EngineCrm
  class ActivitiesController < ApplicationController
    def index
      @activities = Activity.order('created_at desc').limit(20)
    end
  end
end
