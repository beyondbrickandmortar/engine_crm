require_dependency 'engine_crm/application_controller'

module EngineCrm
  class PeopleController < ApplicationController
    before_filter :set_person, only: [:show, :edit, :update, :destroy]

    # GET /people
    def index
      @search = EngineCrm::Person.search(params[:q])
      @people = @search.result
      @search.build_condition if @search.conditions.empty?
      @search.build_sort if @search.sorts.empty?
    end

    # GET /people/1
    def show
    end

    # GET /people/new
    def new
      @person = EngineCrm::Person.new
      @person.contact ||= EngineCrm::Company.find(params[:company_id]) if params[:company_id]
    end

    # GET /people/1/edit
    def edit
    end

    # POST /people
    def create
      @person = EngineCrm::Person.new(person_params)

      if @person.save
        redirect_to :back, notice: 'Person was successfully created.'
      else
        render action: 'new'
      end
    end

    # PATCH/PUT /people/1
    def update
      if @person.update_attributes(person_params)
        redirect_to :back, notice: 'Person was successfully updated.'
      else
        render action: 'edit'
      end
    end

    # DELETE /people/1
    def destroy
      @person.destroy
      redirect_to :back, notice: 'Person was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_person
        @person = EngineCrm::Person.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def person_params
        params[:person]
      end
  end
end
