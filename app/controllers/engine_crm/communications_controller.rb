require_dependency 'engine_crm/application_controller'

module EngineCrm
  class CommunicationsController < ApplicationController
    before_filter :set_communication, only: [:show, :edit, :update, :destroy]
    before_filter :set_company

    # GET /communications
    def index
      @search = scope_communications.search(params[:q])
      @communications = @search.result
      @search.build_condition if @search.conditions.empty?
      @search.build_sort if @search.sorts.empty?
    end

    # GET /communications/1
    def show
    end

    # GET /communications/new
    def new
      @communication = EngineCrm::Communication.new
      @communication.reminder_text = "Call #{EngineCrm::Company.select('name').find(params[:company_id]).name}" if params[:company_id]
      @communication.company ||= EngineCrm::Company.find(params[:company_id]) if params[:company_id]
    end

    # GET /communications/1/edit
    def edit
    end

    # POST /communications
    def create
      @communication = EngineCrm::Communication.new(communication_params)
      if @user
        @communication.user = @user
      end

      if @communication.save
        track_activity @communication
        redirect_to :back, notice: 'Communication was successfully created.'
      else
        render action: 'new'
      end
    end

    # PATCH/PUT /communications/1
    def update
      if @communication.update_attributes(communication_params)
        track_activity @communication
        redirect_to :back, notice: 'Communication was successfully updated.'
      else
        render action: 'edit'
      end
    end

    # DELETE /communications/1
    def destroy
      @communication.destroy
      track_activity @communication
      redirect_to :back, notice: 'Communication was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_communication
        @communication = EngineCrm::Communication.find(params[:id])
      end

    def scope_communications
      if @company
        @company.communications
      else
        EngineCrm::Communication
      end
    end

    def set_company
      @company = EngineCrm::Company.find(params[:company_id]) if params[:company_id]
    end

      # Only allow a trusted parameter "white list" through.
      def communication_params
        params[:communication].reject do |key, val|
          key == :user_id
        end
      end

    def reminder_params
      params[:reminder]
    end
  end
end
