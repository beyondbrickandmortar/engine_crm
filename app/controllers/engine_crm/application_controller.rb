module EngineCrm
  class ApplicationController < ::ApplicationController
    before_filter :set_user

    protected
    def track_activity(trackable)
      a = Activity.new
      a.action = params[:action]
      a.user = @user
      a.trackable = trackable
      a.save
    end

    def set_user
      @user = send(EngineCrm.current_user_method) if EngineCrm.current_user_method
    end
  end
end
