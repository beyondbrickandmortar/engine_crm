require_dependency "engine_crm/application_controller"

module EngineCrm
  class ListsController < ApplicationController
  	before_filter :set_list, only: [:show, :edit, :update, :destroy, :add, :remove]
		before_filter :set_contact, only: [:add, :remove]

  	def index
  		@lists = EngineCrm::List.all
    end

    def show
    end

    def edit
    end

    def update
      if @list.update_attributes(list_params)
        redirect_to :back, notice: 'List was successfully updated.'
      else
        render action: 'edit'
      end
    end

    def new
    	@list = EngineCrm::List.new
    end

    def create
    	@list = EngineCrm::List.new(list_params)

      if @list.save
        redirect_to :back, notice: 'List was successfully created.'
      else
        render action: 'new'
      end
    end

    def add
      @list.contacts << @contact
      @list.save
      @contact.save
      redirect_to :back
    end

    def remove
      @contact.list_id = nil
      @contact.save
      redirect_to :back
    end

    def destroy
      @list.destroy
      redirect_to lists_url, notice: 'List was successfully destroyed.'
    end

    private
    def set_list
    	@list = EngineCrm::List.find(params[:list_id] || params[:id])
    end

    def set_contact
    	@contact = EngineCrm::Contact.find(params[:contact_id])
    end

    # Only allow a trusted parameter "white list" through.
    def list_params
      params[:list]
    end
  end
end
